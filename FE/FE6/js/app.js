const EventHandling = {
    data() {
        return {
            modalclosed: null,
            create: "",

            playlists: [
                {
                    playlist: "Playlist 1"
                },
                {
                    playlist: "Playlist 2"
                },
                {
                    playlist: "Playlist 3"
                },
                {
                    playlist: "Playlist 4"
                }
            ],
            music: [
                {
                    title: "A thousand years",
                    artist: "Christina Peri",
                    album: "some album",
                    duration: "04:30"
                },
                {
                    title: "A thousand years",
                    artist: "Christina Peri",
                    album: "some album",
                    duration: "04:30"
                },
                {
                    title: "A thousand years",
                    artist: "Christina Peri",
                    album: "some album",
                    duration: "04:30"
                },
                {
                    title: "A thousand years",
                    artist: "Christina Peri",
                    album: "some album",
                    duration: "04:30"
                },
                {
                    title: "A thousand years",
                    artist: "Christina Peri",
                    album: "some album",
                    duration: "04:30"
                },
            ],
            test: '',
            musiclist: [],

            title: "",
            artist: "",
            album: "",
            duration: ""

        }

    },
    methods: {
        upload() {

            this.playlists.push({
                playlist: this.create
            })
            document.getElementById("create-playlist").click()
            this.create = ""


        },
        drop(e) {
            e.preventDefault()
            this.musiclist = e.dataTransfer.files


        },
        drag(e) {
            e.preventDefault()
        },
        leave() {

        },

        addmusic() {
            var musicfiles = this.musiclist;
            var jsmediatags = window.jsmediatags;

            var d = this;
            jsmediatags.read(musicfiles[0], {
                onSuccess: function (tag) {
                    var info = tag.tags;
                    console.log(info);
                    if (info.title) {
                        d.title = info.title;


                    } else {
                        d.title = musicfiles[0].name;

                    }
                    if (info.artist) {
                        d.artist = info.artist;

                    }
                    if (info.album) {
                        d.album = info.album;

                    }
                    var src = URL.createObjectURL(musicfiles[0]);
                    var audio = document.querySelector("#sound");
                    audio.src = src;
                    audio.onloadedmetadata = function () {
                        console.log(d.title);
                        
                        d.duration = d.timeduration(audio.duration);
                        d.music.push({
                            title: d.title,
                            artist: d.artist,
                            album: d.album,
                            duration: d.duration
                        })
                         document.querySelector(".cancel").click()
                        
                        d.musiclist = []
                    };

                    console.log(d.title);
                    console.log(d.album);
                    console.log(d.artist);
                },

                onError: function (error) {

                    console.log(error);
                }
            });
        },
         timeduration(duration) {
             
            // Hours, minutes and seconds
            var hrs = ~~(duration / 3600);
            var mins = ~~((duration % 3600) / 60);
            var secs = ~~duration % 60;

            // Output like "1:01" or "4:03:59" or "123:03:59"
            var ret = "";

            if (hrs > 0) {
                ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
            }

            ret += "" + mins + ":" + (secs < 10 ? "0" : "");
            ret += "" + secs;
            return ret;
        }


    },
};

Vue.createApp(EventHandling).mount('#body')
