<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SongControllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
Route::post('/addsong', [SongControllers::class, 'addsong']);
Route::get('/displaysong', [SongControllers::class, 'displaysong']);
Route::post('/deletesong', [SongControllers::class, 'deletesong']);
Route::post('/addplaylist', [SongControllers::class, 'addplaylist']);
Route::get('/displayplaylist', [SongControllers::class, 'displayplaylist']);
Route::post('/deleteplaylist', [SongControllers::class, 'deleteplaylist']);
Route::post('/editplaylist', [SongControllers::class, 'editplaylist']);
Route::post('/addsongtoplaylist', [SongControllers::class, 'addsongtoplaylist']);
Route::get('/displaysongtoplaylist/{id}', [SongControllers::class, 'displaysongtoplaylist']);
Route::post('/deletesongfromplaylist', [SongControllers::class, 'deletesongfromplaylist']);
Route::get('/displaysongnotinplaylist/{id}', [SongControllers::class, 'displaysongnotinplaylist']);





